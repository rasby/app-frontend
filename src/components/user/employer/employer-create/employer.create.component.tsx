import React from "react";
import { Form, Input, Button, message } from "antd";
import { Store } from "antd/lib/form/interface";
import BaseService from "../../../../services/base-service/base.sercive";
import "../style.css";
/**
 * Destructuring Form
 */
const { Item } = Form;
/**
 * @method Component Employer
 */
export const EmployerCreateComponent = () => {
  //Hooks
  const [form] = Form.useForm();
  //Service
  const baseService = new BaseService();
  //Onsubmit
  const handleSubmit = async (values: Store) => {
    values.telefono = parseInt(values.telefono);
    const response = await baseService.Post(values, "empleados/createEmployer");
    if (response) {
      message.info("Empleado creado");
      form.resetFields();
    } else {
      message.error("Error creando empleado");
    }
  };
  return (
    <div className="container-create">
      <h1 className="title-create">Crear Empleado</h1>
      <p className="content-information-create">
        Aqui podra crear añadir un empleado a la constructora{" "}
        <span role="img" aria-label="face">🙂</span>
      </p>
      <Form form={form} onFinish={handleSubmit} layout="vertical">
        {/* Field Email */}
        <Item
          name="correo"
          label="Email"
          rules={[
            { required: true, message: "Campo requirido" },
            {
              type: "email",
              message: "Este campo por ejemplo es contructora@gmail.com",
            },
          ]}
        >
          <Input className="input-form" />
        </Item>
        {/* Field last name */}
        <Item
          name="apellidos"
          label="Apellidos"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <Input className="input-form" />
        </Item>
        {/* Field adress */}
        <Item
          name="direccion"
          label="Direccion"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <Input className="input-form" />
        </Item>
        {/* Field ID */}
        <Item
          name="identificacion"
          label="Identificacion"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <Input className="input-form" />
        </Item>
        {/* Field Names */}
        <Item
          name="nombres"
          label="Nombres"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <Input className="input-form" />
        </Item>
        {/* Field Phone */}
        <Item
          name="telefono"
          label="Telefono"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <Input type="number" className="input-form" />
        </Item>
        {/* Button */}
        <Item>
          <Button type="primary" htmlType="submit" className="button-form">
            Continuar
          </Button>
        </Item>
      </Form>
    </div>
  );
};
