import React, { useEffect, useState } from "react";
import { Space, Table, Button } from "antd";
import { useHistory } from "react-router-dom";
import BaseService from "../../../services/base-service/base.sercive";

/**
 * @method Component
 */
export const WorkViewComponent = () => {
  //Hook
  const [data, setData] = useState<Array<any>>([]);
  const history = useHistory();
  //Service
  const baseService = new BaseService();

  useEffect(() => {
    const getData = async () => {
      setData(await baseService.GetAll("obras/getObras"));
    };
    getData();
  }, []);

  const columns = [
    {
      key: "descripcion",
      title: "Descripcion",
      dataIndex: "descripcion",
      width: 600,
    },
    {
      key: "direccion",
      title: "Direccion",
      dataIndex: "direccion",
    },
    {
      key: "fecha_inicio",
      title: "Fecha inicio",
      dataIndex: "fecha_inicio",
    },
    {
      key: "fecha_fin",
      title: "Fecha fin",
      dataIndex: "fecha_fin",
    },
    {
      key: "nro_contrato",
      title: "Numero de contrato",
      dataIndex: "nro_contrato",
    },
    {
      title: "accion",
      dataIndex: "accion",
      render: (text: any, record: any) =>
        data.length > 0 ? (
          <Space size="middle">
            <Button
              type="primary"
              danger
              onClick={async () => {
                await baseService.Delete(
                  {
                    id_empleado: 1,
                  },
                  `http://localhost:8888/obras/deleteObras/${record.id_obra}`
                );
                history.push("/user");
              }}
            >
              {console.log(record)}
              Eliminar
            </Button>
          </Space>
        ) : null,
    },
  ];
  return (
    <div className="container-employer-view">
      <h1 className="title-employer-view">Lista de Obras</h1>
      <Table columns={columns} dataSource={data.length !== 0 ? data : []} />
    </div>
  );
};
