import React from "react";
import { Form, Input, Button, message, DatePicker } from "antd";
import { Store } from "antd/lib/form/interface";
import BaseService from "../../../services/base-service/base.sercive";
import TextArea from "antd/lib/input/TextArea";
/**
 * Destructuring Form
 */
const { Item } = Form;
/**
 * @method component
 */
export const WorkCreateComponent = () => {
  //Hooks
  const [form] = Form.useForm();
  //Service
  const baseService = new BaseService();
  //Onsubmit
  const handleSubmit = async (values: Store) => {
    if (values.fecha_inicio > values.fecha_fin) {
      message.error("La fecha de inicio es mayor a la fecha fin");
    } else {
      const response = await baseService.Post(values, "obras/createWork");
      if (response) {
        message.info("Obra creada creado");
        form.resetFields();
      } else {
        message.error("Error creando obra");
      }
    }
  }
  return (
    <div className="container-create">
      <h1 className="title-create">Crear Obra</h1>
      <p className="content-information-create">
        Aqui podra añadir una obra a la constructora
        <span role="img" aria-label="face">
          🙂
        </span>
      </p>
      <Form form={form} onFinish={handleSubmit} layout="vertical">
        {/* Field Description */}
        <Item
          name="descripcion"
          label="Descripcion"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <TextArea className="input-form" />
        </Item>
        {/* Field date init */}
        <Item
          name="fecha_inicio"
          label="Fecha inicio"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <DatePicker />
        </Item>
        {/* Field date final */}
        <Item
          name="fecha_fin"
          label="Fecha final"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <DatePicker />
        </Item>
        {/* Field address */}
        <Item
          name="direccion"
          label="Direccion"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <Input className="input-form" />
        </Item>
        {/* Field number */}
        <Item
          name="nro_contrato"
          label="Numero de contrato"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <Input className="input-form" />
        </Item>
        {/* Button */}
        <Item>
          <Button type="primary" htmlType="submit" className="button-form">
            Continuar
          </Button>
        </Item>
      </Form>
    </div>
  );
};
