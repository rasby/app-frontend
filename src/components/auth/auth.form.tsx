import React from "react";
import { Form, Input, Button } from "antd";
import { Store } from "antd/lib/form/interface";
/**
 * Destructuring Form
 */
const { Item } = Form;

/**
 * @method Component Auth Form
 */
export const AuthFormComponent = () => {
  //Hook Form
  const [form] = Form.useForm();
  //Submit
  const handleSubmit = (values: Store) => {
    console.log(values);
  };
  /**
   * Render Form
   */
  return (
    <div className="container-form">
      <h1 className="auth-title">Log in</h1>
      <h2 className="auth-content-app">Aqui te podras loguear en la aplicación de contructora</h2>
      <Form form={form} onFinish={handleSubmit} layout='vertical' >
        {/* Field Email */}
        <Item
          name="emailUser"
          label="Email"
          rules={[
            { required: true, message: "Campo requirido" },
            {
              type: "email",
              message: "Este campo por ejemplo es contructora@gmail.com",
            },
          ]}
        >
          <Input className="input-form" />
        </Item>
        {/* Field Password */}
        <Item
          name="passwordUser"
          label="Password"
          rules={[{ required: true, message: "Campo requirido" }]}
        >
          <Input.Password className="input-form" />
        </Item>
        {/* Button */}
        <Item>
          <Button type="primary" htmlType="submit" className="button-auth">
            Log In
          </Button>
        </Item>
      </Form>
    </div>
  );
};
