import React, { useState, createContext, ReactNode } from "react";

/**
 *@interface 
 */
export default interface IUserContextapp {
  user: any;
  setUser: any;
}
/**
 *@interface
 */
interface IPropsChildren {
  children: ReactNode;
}
/**
 * Context
 */
const ContextApp = createContext({} as IUserContextapp);
/**
 * @Context
 */
const UserProvider = ({ children }: IPropsChildren) => {
  const [user, setUser] = useState();
  return (
    <ContextApp.Provider value={{ user, setUser }}>
      {children}
    </ContextApp.Provider>
  );
};

export {ContextApp, UserProvider}
