import React from "react";
import "./App.css";
import { UserComponent } from "./components/user/user.component";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { EmployerCreateComponent } from "./components/user/employer/employer-create/employer.create.component";
import { AuthComponent } from "./components/auth/auth.component";
import { UserProvider } from "./user/user.context";

function App() {
  return (
    <UserProvider>
      <Router>
        <Switch>
          <Route exact path="/user" component={UserComponent} />
          <Route exact path="/" component={AuthComponent} />
          <Route
            exact
            path="/createEmployer"
            component={EmployerCreateComponent}
          />
        </Switch>
      </Router>
    </UserProvider>
  );
}
export default App;
