/***
 * URL API principal
 */
const BACKEND_API: string = "http://localhost:8888";
/**
 * @class Base service
 * This class will be in charge of containing the http logic for the call of the api of de spring boot
 */
export default class BaseService<T> {
  /***
   * @method Post
   */
  async Post(body: T, url: string) {
    return fetch(`${BACKEND_API}/${url}`, {
      headers: {
        "Content-type": "application/json",
      },
      method: "POST",
      mode: "cors",
      body: JSON.stringify(body as any),
    });
  }
  /***
   * @method Post
   */
  async Delete(body: T, url: string) {
    return fetch(url, {
      headers: {
        "Content-type": "application/json",
      },
      method: "DELETE",
      mode: "cors",
      body: JSON.stringify(body as any),
    });
  }

  /***
   * @method Get
   */
  async GetAll(url: string) {
    return await (
      await fetch(`${BACKEND_API}/${url}`, {
        method: "GET",
      })
    ).json();
  }
  /***
   * @method Get By Body
   */
  async GetByBody(body: T, url: string) {
    return await (
      await fetch(`${BACKEND_API}/${url}`, {
        method: "GET",
        body: body as any,
      })
    ).json();
  }
}
